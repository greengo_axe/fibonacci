# Fibonacci's suquence

## Abstract 
Fibonacci's sequence through YII2.  Data are stored into MySQL/MariaDb database.

## Premise
This project requires :

* PHP 7.x
* MySql/MariaDb 5.x
* Composer

They have to be up and running. 

## Installation
*  Download and install Yii2 framework(advanced template), via composer, on your local computer . For all info click [here](https://yiiframework.com) 
 *  The root folder of the project is 
 
       >**fibonacci** 
    
 * and the absolute path is
 
      >**/var/www/html/fibonacci**
    
  * Clone the project from gitlab on Desktop :
    
    >**git@gitlab.com:greengo_axe/fibonacci.git**

* copy and overwrite all folders and files from 

>>**Desktop/fibonacci**

>into 

>>**/var/www/html/fibonacci**

* Delete the folder on Desktop (**Desktop/fibonacci**)

* Change the permissions:
> **cd /var/www/html/**
> 
> **chmod -R 777 fibonacci**

* Import the database structure (**fibonacci/fibonacci.sql**) into MySQL/MariaDb via PHPMyAdmin or via console
> /var/www/html/fibonacci/config/db.php # db settings

* Start the YII's built-in server: 

> **cd fibonacci**

> **php yii serve**

* Open your favourite browser and insert this URL:

> **localhost:8080**

* Enjoy!


## Troubleshooting
  * This project has been developed on Linux OS and there might be a few problems with the path or to grant the right permissions on Windows / Apple OSs.
