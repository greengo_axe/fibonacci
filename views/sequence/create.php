<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sequence */

$this->title = Yii::t('app', 'Start a new sequence');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'sequences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sequence-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
