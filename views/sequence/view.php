<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sequence */

$this->title = 'max value: '.$model->max_value;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sequences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="sequence-view">
    
<?php
    echo Html::a(Yii::t('app', 'Start a new sequence'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1>The sequence with <?= Html::encode($this->title) ?></h1>
    <p>
        <h2>
            <?= Html::encode($sequence) ?>
        </h2>
    </p>
</div>
