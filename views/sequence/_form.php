<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sequence */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sequence-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'max_value')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Start'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
