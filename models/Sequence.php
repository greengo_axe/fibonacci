<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sequence".
 *
 * @property int $id
 * @property int $max_value
 */
class Sequence extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sequence';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['max_value'], 'required'],
            [['max_value'], 'integer','min' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'id' => Yii::t('app', 'ID'),
            'max_value' => Yii::t('app', 'Max Value'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return SequenceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SequenceQuery(get_called_class());
    }
}
