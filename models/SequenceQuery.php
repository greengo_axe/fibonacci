<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Sequence]].
 *
 * @see Sequence
 */
class SequenceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Sequence[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Sequence|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
