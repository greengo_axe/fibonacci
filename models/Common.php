<?php

namespace app\models;

class Common  {
     
    /**
     * 
     * @param int $max_value
     * @return string
     */
     public function fibonacciSequence($max_value)
    {
         $a = 0;
         $b = 1;
         $c = 0;
         $sequence = '';
         
         while ($a<=$max_value){
             $sequence .= $a.', ';
             $c = $a;
             $a = $b;
             $b = $c+$b;
         }
         
         return $sequence;
    }
}
